#!/usr/bin/env bash



# Create initial superuser
python manage.py shell <<EOF

from django.contrib.auth.models import User

user = User.get_user_model()
user.objects.create_superuser('$SUPER_USERNAME', '$SUPER_EMAIL', '$SUPER_PASSWORD')

exit()

EOF