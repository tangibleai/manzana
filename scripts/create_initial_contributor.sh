#!/usr/bin/env bash

# Create initial contributor (superuser)

python manage.py shell <<EOF

from users.models import Contributor

Contributor.objects.create_superuser(
    username = '$SUPER_USERNAME',
    email = '$SUPER_EMAIL',
    password = '$SUPER_PASS'
)

exit()

EOF


