#!/usr/bin/env bash



# Create initial data
python manage.py shell <<EOF

import yaml, ast

from users.models import Contributor
from ideas.models import Idea, Task
from tags.models import Tag


user, created = Contributor.objects.get_or_create(username = 'hobs')
tag, created = Tag.objects.get_or_create(name = 'none', about = 'none')

unique_tags = []

with open('data/ideas.yml') as f:
	idea_yaml = yaml.safe_load(f)

for idea in idea_yaml:
	obj, bool = Idea.objects.get_or_create(
		name = idea['name'],
		draft = [task for task in idea['tasks']],
		description = """
		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse 
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
		"""
	)
	obj.contributors.set([user])
	obj.tags.set([tag])

ideas = Idea.objects.all()

for idea in ideas:
	tasks = ast.literal_eval(idea.draft)
	for task in tasks:
		item = Task.objects.get_or_create(idea = idea, name = task)
		

for idea in idea_yaml:
	try:
		tags = idea['tags']
		for tag in tags:
			if tag not in unique_tags:
				unique_tags.append(tag)
				item = Tag.objects.get_or_create(name = tag, about = '.')
	except:
		pass

exit()

EOF