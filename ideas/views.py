from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.generic import ListView, DetailView

from .models import Idea, Task



#@login_required

"""
def home(request):

    data = {
        'ideas': Idea.objects.all(),
        'tasks': Task.objects.all()
    }
    return render(request, 'ideas/home.html', data)
"""

class IdeaListView(ListView):
    """list/read the Idea instances"""

    model = Idea  
    template_name = 'ideas/home.html'
    context_object_name = 'ideas'
    ordering = ['-date_created']

class IdeaDetailView(DetailView):
    """list/read a single Idea instance"""

    model = Idea 
    




def about(request):
    return render(request, 'ideas/about.html', {'title': 'about'})









""" Concrete View Classes
# CreateAPIView
Used for create-only endpoints.
# ListAPIView
Used for read-only endpoints to represent a collection of model instances.
# RetrieveAPIView
Used for read-only endpoints to represent a single model instance.
# DestroyAPIView
Used for delete-only endpoints for a single model instance.
# UpdateAPIView
Used for update-only endpoints for a single model instance.
# ListCreateAPIView
Used for read-write endpoints to represent a collection of model instances.
RetrieveUpdateAPIView
Used for read or update endpoints to represent a single model instance.
# RetrieveDestroyAPIView
Used for read or delete endpoints to represent a single model instance.
# RetrieveUpdateDestroyAPIView
Used for read-write-delete endpoints to represent a single model instance.
"""