from django.urls import path

#from . import views
from .views import IdeaListView, IdeaDetailView, about



urlpatterns = [
    path('', IdeaListView.as_view(), name = 'ideas-home'),
    path('ideas/<int:pk>/', IdeaDetailView.as_view(), name = 'idea-detail'),
    path('about/', about, name = 'ideas-about')
]
