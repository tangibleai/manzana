from django.test import TestCase 
from django.urls import reverse



class HomePageTests(TestCase):

	def test_url_exists_at_correct_location(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_url_available_by_name(self):
		response = self.client.get(reverse('ideas-home'))
		self.assertEqual(response.status_code, 200)
	
	def test_template_name_correct(self):
		response = self.client.get('/')
		self.assertTemplateUsed(response, 'ideas/home.html')


class AboutPageTests(TestCase):

	def test_url_exists_at_correct_location(self):
		response = self.client.get('/about/')
		self.assertEqual(response.status_code, 200)

	def test_url_available_by_name(self):
		response = self.client.get(reverse('ideas-about'))
		self.assertEqual(response.status_code, 200)

	def test_template_name_correct(self):
		response = self.client.get('/about/')
		self.assertTemplateUsed(response, 'ideas/about.html')
	