from django.test import TestCase 
from django.urls import reverse



class TagPageTests(TestCase):

	def test_url_exists_at_correct_location(self):
		response = self.client.get('/tags/')
		self.assertEqual(response.status_code, 200)

	def test_url_available_by_name(self):
		response = self.client.get(reverse('ideas-tags'))
		self.assertEqual(response.status_code, 200)
	
	def test_template_name_correct(self):
		response = self.client.get('/tags/')
		self.assertTemplateUsed(response, 'tags/tags.html')

	