from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from .models import Tag



@login_required
def tags(request):
		
	data = {
		'tags': Tag.objects.all()
	}
	#return render(request, 'tags/tags.html', {'title': 'tags'})
	return render(request, 'tags/tags.html', data)