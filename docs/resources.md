# Useful Learning Resources

## Python

* [PEP 8 – Style Guide for Python Code](https://peps.python.org/pep-0008/)

## Django

* [Django series playlist by Corey Schafer](https://www.youtube.com/playlist?list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4p)

* [Django tutorial by Will Vincent](https://learndjango.com/)

* [Django Best Practices: Custom User Model by Will Vincent](https://learndjango.com/tutorials/django-custom-user-model)

* [Django shortcuts – get_or_create and update_or_create by Manuel Matosevic](https://zerotobyte.com/django-shortcuts-get-or-create-and-update-or-create/)

## Django Testing

* [Django testing tutorial by Will Vincent](https://learndjango.com/tutorials/django-testing-tutorial)

* [Django Tutorial Part 10: Testing a Django web application by Mozilla](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Testing)

* [Testing in Django (Part 1) – Best Practices and Examples](https://realpython.com/testing-in-django-part-1-best-practices-and-examples/)

## GitLab CI/CD

* [GitLab CI to Test and Deploy Django REST API by Mohammed R. Ananda](https://medium.com/@mridho2828/gitlab-ci-to-test-and-deploy-django-rest-api-297618cb2d90)

* [Get started with GitLab CI/CD by GitLab](https://docs.gitlab.com/ee/ci/quick_start/)

* [`.gitlab-ci.yml` keyword reference](https://docs.gitlab.com/ee/ci/yaml/index.html)

* [GitLab CI CD Pipeline Tutorial by Tech and Beyond With Moss](https://www.youtube.com/watch?v=mnYbOrj-hLY&t=1853s&ab_channel=TechandBeyondWithMoss)

## Render Deployment

* [Getting Started with Django on Render](https://render.com/docs/deploy-django)

* [Deployment checklist](https://docs.djangoproject.com/en/4.0/howto/deployment/checklist/)

* [How To Deploy a Django App to Render.com](https://www.swyx.io/django-on-render)

