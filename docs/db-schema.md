# Database Schema

## Users table (profile)

|     | Users        |
|:----|:-------------|
| Pk  | user_id      |
|     | first_name   | 
|     | last_name    |
|     | username     | 
|     | email        |
|     | picture      |
|     | password     |
|     | date_joined  |
 


## Ideas table (entity relationship with Users and Tags tables)

&rarr; Users table


|     | Ideas        |                             |
|:----|:-------------|:----------------------------|
| Pk  | idea_id      |  (integer NOT NULL)         |
| Fk  | user_id      |  (integer NOT NULL)         | 
|     | name         |  (varchar(100) NOT NULL)    |
|     | content      |  (text NOT NULL)     	   | 
| Fk  | tag_id       |                             |
|     | date_created |  (datetime NOT NULL)        |



## Tag table



|     | Ideas        |                              |
|:----|:-------------|:-----------------------------|
| Pk  | tag_id       | integer NOT NULL             |
|     | name         | varchar(50) NOT NULL UNIQUE  |
|     | content      | text NOT NULL                |
