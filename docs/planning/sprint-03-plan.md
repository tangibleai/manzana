# Sprint 3 (Jun 10 - Jun 17)

* [x] R1-1: Setup a new server and database on render
* [x] R1-1: Create manually a new YAML file for contributors
* [x] R2-2: Create a custom user model named Contributor
* [x] R1-1: Change models.Idea.author to contributor (many to many)
* [x] R1-2: Fix the import bugs and migrate
* [x] R2-1: Create a Python script to load the contributors 
* [x] R2-1: Write simple unit tests for the user views
* [x] R3-2: Write simple unit tests for the user model
* [x] R2-2: Create a new model named Profile
* [x] R2-2: Allow users to cretae and update their profile info
* [x] R2-2: Add signals to create profiles automatically
* [] R5: Design the user interface with draw.io

## Backlog

* [] R1: Get Sendgrid account info from Hobson
* [] H1: Have hobson add the domain name
* [] R2: Run the first CI/CD pipeline (verfication needed)



## Done Sprint 2 (Jun 03 - Jun 10)

* [x] R2-2: Debug the database reset on production
* [x] R1-1: Create a Task table within ideas app
* [x] R1-2: Represent one-to-many relationship (Task <-> Idea)
* [x] R1-1: Update the Tag table 
* [x] R1-1: Represent one-to-many relationship (Idea <-> Tag)
* [x] R1-1: Load the YAML file via terminal/Python
* [x] R2-1: Load the name from the YAML file to Idea name field
* [x] R1-1: Load the tasks from the YAML file to Idea content field
* [x] R2-2: Load the tasks from Idea content field to Task table
* [x] R2-2: Load the tasks of every idea on the home page
* [x] R1-1: Add many to many relationship (Idea <-> Tag)
* [x] R1-1: Load the tags from the YAML file to Tag table
* [x] R1-1: Load the tags of every idea on the home page
* [x] R2-1: Run the script within the build script
* [x] R2-3: Create a user registration page
* [x] R2-1: Create an authentication system
* [x] R2-1: restrict access to the home and tags page
* [x] R2-2: Add password reset feature
* [x] R2-1: Configure the application for Sendgrid






