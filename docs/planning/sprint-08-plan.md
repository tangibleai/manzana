# Sprint 8 (Jul 15 - Jul 22)

* [x] R1-1: Restructure the `main` branch to fit single-server approach
* [x] R1-1: Set up a new PostgreSQL database on Render 
* [x] R1-1: Create a CustomUser model named **Contributor**
* [x] R1-1: Create a **Profile** model that inherits from Contributor table
* [x] R1-1: Add signals to create profiles automatically once the User is created.
* [x] R2-2: Create a custom register endpoint 
* [x] R1-1: Implement the authentication endpoints using Django auth views
* [x] R2-2: Add the reset password endpoint to the authentication
* [x] R2-3: Customize the form inputs using CSS and Bootstrap 5 (no crispy-forms)
* [x] R1-1: Create a class-based endpoint to read the ideas
* [x] R1-2: Create a class-based endpoint to read one single idea
* [] R2: Create an endpoint to create a new idea
* [] R2: Create an endpoint to update one single idea

## Backlog

* [] R?: Update the Idea name and Idea description 
* [] R?: Create a new Idea based on Idea table
* [] R1: Get Sendgrid account info from Hobson
* [] R1: Make django settings for development and deployment
* [] R2: Run the first CI/CD pipeline (verfication needed)

## Done Sprint 7 (Jul 08 - Jul 15)

* [x] R1-1: Create a new branch named **staging** based on **monolithic**
* [x] R1-1: Remove unnecessary code and files
* [x] R2-1: Update the branch for render deployment
* [x] R1-1: Deploy the branch to **ideas.qary.ai**
* [x] R1-1: Serialize the custom User model 
* [x] R1-1: Set up a new Svelte project `client`
* [x] R1-1: Create a markup to display the users list
* [x] R1-1: Pass a fetch to get the users list
* [x] R?: Push and deploy everytime I make a change
