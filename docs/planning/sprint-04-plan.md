# Sprint 4 (Jun 17 - Jun 24)

* [x] R2-2: Take a course on draw.io
* [x] R5-9: Design the user interface with draw.io
* [x] R1-1: Create an interactive prototype for the wireframe
* [x] R1-2: Add a color scheme to the wireframe
* [x] R2-2: Search & decide on the front-end stack 
* [x] R1-1: Share the user interface design with SDPG/qary

## Backlog

* [] R1: Get Sendgrid account info from Hobson
* [x] H1-2: Have hobson add the domain name
* [] R1: Make django settings for development and deployment
* [] R2: Run the first CI/CD pipeline (verfication needed)

## Done Sprint 3 (Jun 10 - Jun 17)

* [x] R1-1: Setup a new server and database on render
* [x] R1-1: Create manually a new YAML file for contributors
* [x] R2-2: Create a custom user model named Contributor
* [x] R1-1: Change models.Idea.author to contributor (many to many)
* [x] R1-2: Fix the import bugs and migrate
* [x] R2-1: Create a Python script to load the contributors 
* [x] R2-1: Write simple unit tests for the user views
* [x] R3-2: Write simple unit tests for the user model
* [x] R2-2: Create a new model named Profile
* [x] R2-2: Allow users to cretae and update their profile info
* [x] R2-2: Add signals to create profiles automatically
