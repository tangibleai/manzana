# Sprint 5 (Jun 24 - Jul 01)

* [x] R1-1: Transform the Contributor model into a Restful API
* [x] R1-3: Understand the Django authentication approaches
* [x] R1-1: Update the default authentication settings to Token authentication
* [x] R1-1: Create endpoints for users to authenticate
* [x] R2-2: Write one unit test/doctest for some aspect of authentication
* [x] R3-5: Build the landing and the authentication pages with Svelte
* [x] R2-3: Create a register endpoint and fetch data 
* [x] R2-2: Create a login endpoint and fetch data
* []  R2: Create a logout endpoint and fetch data

## Backlog

* [] R1: Get Sendgrid account info from Hobson
* [] R1: Make django settings for development and deployment
* [] R2: Run the first CI/CD pipeline (verfication needed)

## Done Sprint 4 (Jun 17 - Jun 24)

* [x] R2-2: Take a course on draw.io
* [x] R5-9: Design the user interface with draw.io
* [x] R1-1: Create an interactive prototype for the wireframe
* [x] R1-2: Add a color scheme to the wireframe
* [x] R2-2: Search & decide on the front-end stack 
* [x] R1-1: Share the user interface design with SDPG/qary
