# Sprint 2 (Jun 03 - Jun 10)

* [x] R2-2: Debug the database reset on production
* [x] R1-1: Create a Task table within ideas app
* [x] R1-2: Represent one-to-many relationship (Task <-> Idea)
* [x] R1-1: Update the Tag table 
* [x] R1-1: Represent one-to-many relationship (Idea <-> Tag)
* [x] R1-1: Load the YAML file via terminal/Python
* [x] R2-1: Load the name from the YAML file to Idea name field
* [x] R1-1: Load the tasks from the YAML file to Idea content field
* [x] R2-2: Load the tasks from Idea content field to Task table
* [x] R2-2: Load the tasks of every idea on the home page
* [x] R1-1: Add many to many relationship (Idea <-> Tag)
* [x] R1-1: Load the tags from the YAML file to Tag table
* [x] R1-1: Load the tags of every idea on the home page
* [x] R2-1: Run the script within the build script
* [x] R2-3: Create a user registration page
* [x] R2-1: Create an authentication system
* [x] R2-1: restrict access to the home and tags page
* [x] R2-2: Add password reset feature
* [x] R2-1: Configure the application for Sendgrid

## Backlog

* [] R1: Get sendgrid account credentials from Honson
* [] R1: Send Hobson the IP address
* [] H1: Have hobson add the domain name
* [x] R2-2: Search for a good Figma open-source alternative
* [] R5: Draw a wireframe in figma open source alternative 
* [x] R2-1: Come up with a tech stack recommendation
* [] RH1: Decide on the tech stack
* [] R2: Run the first CI/CD pipeline (verfication needed)


## Done Sprint 1 (May 27 - Jun 03)

* [x] H2: Create a YAML file for project ideas
* [x] R?: Clone the repository
* [x] R?: Copy **requirements.txt** and **.gitignore** from `nudger` and push to main
* [x] R?: Watch first Corey Schafer video and implement it
* [x] R?: Add home and about views to the app 
* [x] R?: Read a tutorial on Django tests
* [x] R?: Create a very simple test and run it locally
* [x] R?: Read/watch a tutorial on Gitlab CI/CD - part 1
* [x] R?: Add the test to **.gitlab-ci.yml**
* [x] R?: Practice Gitlab CI/CD different repository - part 1
* [x] R?: Create a simple CI/CD pipeline 
* [x] R?: Structure the initial database in django based on the YAML file 
* [x] H?: Invite rochdi to (render)[render.com] 
* [x] R?: Sign up for (render)[render.com] account (email from hobson)
* [x] R?: Practice Django render deployment on different repo 
* [x] R?: Update the app for render deployment 
* [x] R?: Try to deploy the admin page to subdomain.onrender.com domain
* [x] R?: Automate the superuser creation using the  build script
* [x] R?: Try to fix the djanog admin authentication bug 
* [x] R?: Add resources directory for useful resources/tutorial/blogs... 
* [x] R?: Structure the database schema using the YAML file 






