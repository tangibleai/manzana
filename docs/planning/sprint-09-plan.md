# Sprint 9 (Jul 22 - Jul 29)

* [] R1: Create an endpoint to create a new Idea instance
* [] R2: Create "new idea" page to create a new idea
* [] R1: Create an endpoint to update one single Idea instance
* [] R1: Create "update" page to update one single idea
* [] R1: Add an endpoint `/contributors`to read contributors

## Backlog

* [] R1: Get Sendgrid account info from Hobson
* [] R1: Make django settings for development and deployment
* [] R2: Run the first CI/CD pipeline (verfication needed)

## Done Sprint 8 (Jul 15 - Jul 22)

* [x] R1-1: Restructure the `main` branch to fit single-server approach
* [x] R1-1: Set up a new PostgreSQL database on Render 
* [x] R1-1: Create a CustomUser model named **Contributor**
* [x] R1-1: Create a **Profile** model that inherits from Contributor table
* [x] R1-1: Add signals to create profiles automatically once the User is created.
* [x] R2-2: Create a custom register endpoint 
* [x] R1-1: Implement the authentication endpoints using Django auth views
* [x] R2-2: Add the reset password endpoint to the authentication
* [x] R2-3: Customize the form inputs using CSS and Bootstrap 5 (no crispy-forms)
* [x] R1-1: Create a class-based endpoint to read the ideas
* [x] R1-2: Create a class-based endpoint to read one single idea