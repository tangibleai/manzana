# Sprint 7 (Jul 08 - Jul 15)

> I'm going to push/deploy one change at a time

* [x] R1-1: Create a new branch named **staging** based on **monolithic**
* [x] R1-1: Remove unnecessary code and files
* [x] R2-1: Update the branch for render deployment
* [x] R1-1: Deploy the branch to **ideas.qary.ai**
* [x] R1-1: Serialize the custom User model 
* [x] R1-1: Set up a new Svelte project `client`
* [x] R1-1: Create a markup to display the users list
* [x] R1-1: Pass a fetch to get the users list
* [x] R?: Push and deploy everytime I make a change

> Some additional tasks can be added to the list

## Backlog

* [] R?: Update the Idea name and Idea description 
* [] R?: Create a new Idea based on Idea table
* [] R1: Get Sendgrid account info from Hobson
* [] R1: Make django settings for development and deployment
* [] R2: Run the first CI/CD pipeline (verfication needed)

## Done Sprint 6 (Jun 01 - Jul 08)

* [x] R2-2: Logout the user by sending cookies via an HTTP POST request
* [x] R1-2: Change the navbar when the user is authenticated
* [x] R3-4: Create a custom reset-password-request endpoint that:
        * checks if the email exists in the database
        * throws an error when user's email is not existed
        * sends a mail if the email existed
* [x] R3-3: Create a custom reset-password endpoint that:
        * throws an error when user is not existed
        * verifies the generated token
        * resets password
* [x] R1-1: Pass a fetch to consume the reset-password request endpoint
* [] R1: Pass a fetch to consume the reset-password endpoint
* [x] R2-2: Build the home page with Svelte
* [x] R1-1: Transform the Idea table to a Restful API
* [x] R2-1: Read the Idea name and Idea description from Idea table
