# Sprint 1 (May 27 - Jun 03)

* [x] H2: Create a YAML file for project ideas
* [x] R?: Clone the repository
* [x] R?: Copy **requirements.txt** and **.gitignore** from `nudger` and push to main
* [x] R?: Watch first Corey Schafer video and implement it
* [x] R?: Add home and about views to the app 
* [x] R?: Read a tutorial on Django tests
* [x] R?: Create a very simple test and run it locally
* [x] R?: Read/watch a tutorial on Gitlab CI/CD - part 1
* [x] R?: Add the test to **.gitlab-ci.yml**
* [x] R?: Practice Gitlab CI/CD different repository - part 1
* [x] R?: Create a simple CI/CD pipeline 
* [x] R?: Structure the initial database in django based on the YAML file 
* [x] H?: Invite rochdi to (render)[render.com] 
* [x] R?: Sign up for (render)[render.com] account (email from hobson)
* [x] R?: Practice Django render deployment on different repo 
* [x] R?: Update the app for render deployment 
* [x] R?: Try to deploy the admin page to subdomain.onrender.com domain
* [x] R?: Automate the superuser creation using the  build script
* [] RH1: send H the IP address and have hobson add domain name idea.qary.ai

## Backlog

* [x] R?: Add resources directory for useful resources/tutorial/blogs... 
* [] R?: Draw a wireframe in figma open source alternative or drawio
* [x] R?: Structure the database schema using the YAML file 
* [x] R?: Try to fix the djanog admin authentication bug 
* [] RH?: Decide on tech stack (celery rmq postgres crispy-forms svelte bootstrap)
* [] R?: Run the first CI/CD pipeline (verification needed)

