# Sprint 6 (Jul 01 - Jul 08)

* [x] R2-2: Logout the user by sending cookies via an HTTP POST request
* [x] R1-2: Change the navbar when the user is authenticated
* [x] R3-4: Create a custom reset-password-request endpoint that:
        * checks if the email exists in the database
        * throws an error when user's email is not existed
        * sends a mail if the email existed
* [x] R3-3: Create a custom reset-password endpoint that:
        * throws an error when user is not existed
        * verifies the generated token
        * resets password
* [x] R1-1: Pass a fetch to consume the reset-password request endpoint
* [] R1: Pass a fetch to consume the reset-password endpoint
* [x] R2-2: Build the home page with Svelte
* [x] R1-1: Transform the Idea table to a Restful API
* [x] R2-1: Read the Idea name and Idea description from Idea table
* [] R?: Update the Idea name and Idea description 
* [] R?: Create a new Idea based on Idea table

## Backlog

* [] R1: Get Sendgrid account info from Hobson
* [] R1: Make django settings for development and deployment
* [] R2: Run the first CI/CD pipeline (verfication needed)

## Done Sprint 5 (Jun 24 - Jul 01)

* [x] R1-1: Transform the Contributor model into a Restful API
* [x] R1-3: Understand the Django authentication approaches
* [x] R1-1: Update the default authentication settings to Token authentication
* [x] R1-1: Create endpoints for users to authenticate
* [x] R2-2: Write one unit test/doctest for some aspect of authentication
* [x] R3-5: Build the landing and the authentication pages with Svelte
* [x] R2-3: Create a register endpoint and fetch data 
* [x] R2-2: Create a login endpoint and fetch data
