from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

#from .forms import ContributorCreationForm, ContributorChangeForm
from .models import Contributor, Profile 



class ContributorAdminConfig(BaseUserAdmin):
    """Customizes fields to add/change Contributor instances"""

    #add_form = ContributorCreationForm
    #form = ContributorChangeForm

    model = Contributor
    # fields to be used in displaying the Contributor model
    list_display = [
        'id',
        'email',
        'username',
        'is_active',
        'is_staff',
        'is_superuser'
    ]
    # fields to be used in changing/updating a user
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('username',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser')})
    )
    # fields to be used in creating a new user
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'password1', 'password2'),
        }),
    )

    ordering = ('id',)


class ProfileAdmin(admin.ModelAdmin):
    """Customizes fields to add/change Profile instances"""
    
    # fields to be used in displaying the Profile model
    list_display = [
        'contributor',
        'picture'
    ]


admin.site.register(Contributor, ContributorAdminConfig)
admin.site.register(Profile, ProfileAdmin)

