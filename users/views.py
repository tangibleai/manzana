from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from .forms import (
    ContributorCreationForm, ContributorUpdateForm, ProfileUpdateForm
)



def register(request):

    if request.user.is_authenticated:
        return redirect('ideas-home')

    if request.method == 'POST':

        form = ContributorCreationForm(request.POST)

        if form.is_valid():

            form.save()
            return redirect('login')

    else:
        form = ContributorCreationForm()

    return render(request, 'users/register.html', {'form': form})


@login_required
def profile(request):

    if request.method == 'POST':
        c_form = ContributorUpdateForm(request.POST, instance = request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance = request.user.profile)

        if c_form.is_valid() and p_form.is_valid():
            c_form.save()
            p_form.save()
            return redirect('profile')
    else:
        c_form = ContributorUpdateForm(instance = request.user)
        p_form = ProfileUpdateForm(instance = request.user.profile) 

    context = {
        'c_form': c_form,
        'p_form': p_form
    }

    return render(request, 'users/profile.html', context)

