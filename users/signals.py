from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Contributor, Profile 



@receiver(post_save, sender = Contributor)
def create_profile(sender, instance, created, **kwargs):
    """Creates a Profile instance once the user gets saved"""
    if created:
        Profile.objects.create(contributor = instance)

@receiver(post_save, sender = Contributor)
def save_profile(sender, instance, **kwargs):
    """Saves the Profile instance"""
    instance.profile.save()
