from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from PIL import Image

from django.db import models  



class ContributorManager(BaseUserManager):
    """A class to manage user accounts"""

    def create_user(self, email, username, password = None):
        """Creates and saves a user with the given email, name and password"""
        if not email:
            raise ValueError('Users should have an email address')
        user = self.model(
            email = self.normalize_email(email),
            username = username,
        )
        user.set_password(password)
        user.save(using = self._db)
        return user

    def create_superuser(self, email, username, password = None):
        """Creates and saves a superuser with the given email, name and password"""

        user = self.create_user(
            email,
            username = username,
            password = password
        )
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using = self._db)
        return user

class Contributor(AbstractBaseUser, PermissionsMixin):
    """A class to create a contributor account"""

    email = models.EmailField(max_length = 255, unique = True)
    username = models.CharField(max_length = 150, unique = True)
    is_active = models.BooleanField(default = True)
    is_staff = models.BooleanField(default = False)
    is_superuser = models.BooleanField(default = False)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    objects = ContributorManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.username 

    class Meta:
        verbose_name_plural = 'contributors'


class Profile(models.Model):
    """A class to create a contributor profile"""

    contributor = models.OneToOneField(Contributor, on_delete = models.CASCADE)
    picture = models.ImageField(default = 'default.jpg', upload_to = 'profile_pics')

    def __str__(self):
        return f"{self.contributor.username}'s profile"

    def save(self, *args, **kwargs):
        """Saves and resizes contributor's profile picture"""
        super().save()

        pic = Image.open(self.picture.path)

        if pic.height > 300 or pic.width > 300:
            output_size = (300, 300)
            pic.thumbnail(output_size)
            pic.save(self.picture.path)

